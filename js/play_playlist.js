/**
 * @file
 * Player initialization for embedded playlists.
 *
 * https://developer.brid.tv/docs/playlist-widget-parameters.
 */

window.bridPlayStack = window.bridPlayStack || [];
// http://developer.brid.tv/brid-player/code-examples/basic
window.document.blockSimultaneousPlay = true;

(function (playStack, Drupal) {

    /**
     * Initialize players for all playlists.
     *
     * @type {{attach: Drupal.bridPlayPlaylist.attach, waitForLoad: Drupal.bridPlayPlaylist.waitForLoad}}
     */
    Drupal.behaviors.bridPlayPlaylist = {
        attach: function (context, settings) {
            this.waitForLoad(function (playStack) {
                var buffer;
                while (playStack.length > 0) {
                    buffer = playStack.shift();
                    window.$bWidgets.init(buffer);
                }
            });
        },

        waitForLoad: function (callback) {
            var self = this;
            if (window.$bWidgets) {
                callback.call(this, playStack);
            } else {
                setTimeout(function () {
                    self.waitForLoad(callback);
                }, 15);
            }
        }
    }

}(window.bridPlayStackPlaylist, window.Drupal));

window.Drupal.behaviors.bridPlayPlaylist.attach();
