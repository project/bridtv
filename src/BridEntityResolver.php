<?php

namespace Drupal\bridtv;

use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * The Brid Entity Resolver.
 */
class BridEntityResolver implements BridEntityResolverInterface {

  const ENTITY_TYPE = 'media';

  const BUNDLE = 'bridtv';

  const FIELD = 'field_bridtv';

  /**
   * An array of types.
   *
   * @var array
   */
  protected $cached;

  /**
   * The cache reset counter.
   *
   * @var int
   */
  protected $cacheResetCounter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity cache.
   *
   * @var \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface
   */
  protected $entityMemoryCache;

  /**
   * The type of entity (video OR playlist)
   *
   * @var string
   */
  protected $type;

  /**
   * BridEntityResolver constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $entity_memory_cache
   *   The entity memory cache.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MemoryCacheInterface $entity_memory_cache) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityMemoryCache = $entity_memory_cache;
    $this->type = 'video';
    $this->cached = [$this->type => []];
    $this->cacheResetCounter = 0;
  }

  /**
   * Get the representing entity for the given video id.
   *
   * @param int $id
   *   The Brid.TV video id.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface|null
   *   The entity if existent, or NULL if not existent.
   */
  public function getEntityForVideoId($id) {
    $storage = $this->getEntityStorage();
    if (!isset($this->cached[$this->type][$id])) {
      $this->usingInternalCache();
      $query = $storage->getQuery();
      $query->condition(static::FIELD . '.' . $this->type . '_id', $id, '=');
      $query->range(0, 1);
      $query->accessCheck(TRUE);
      if ($result = $query->execute()) {
        $this->cached[$this->type][$id] = reset($result);
      }
    }
    return isset($this->cached[$this->type][$id]) ? $storage->load($this->cached[$this->type][$id]) : NULL;
  }

  /**
   * Creates a new entity instance.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   *   A new entity, unsaved, without any field values.
   */
  public function newEntity() {
    $storage = $this->getEntityStorage();
    $definition = $this->getEntityTypeDefinition();
    $bundle_key = $definition->getKey('bundle');
    return $storage->create([$bundle_key => static::BUNDLE, 'uid' => 1]);
  }

  /**
   * Gets a entity type definition.
   *
   * @return mixed
   *   A plugin definition, or NULL if the plugin ID is invalid and
   *   $exception_on_invalid is FALSE.
   */
  public function getEntityTypeDefinition() {
    return $this->entityTypeManager->getDefinition(static::ENTITY_TYPE);
  }

  /**
   * Gets the entity storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   Returns a EntityStorageInterface.
   */
  public function getEntityStorage() {
    return $this->entityTypeManager->getStorage(static::ENTITY_TYPE);
  }

  /**
   * Gets the entity query.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   Returns a QueryInterface.
   */
  public function getEntityQuery() {
    $query = $this->getEntityStorage()->getQuery();
    $query->accessCheck(TRUE);
    $query->condition($this->getEntityTypeDefinition()
      ->getKey('bundle'), static::BUNDLE, '=');
    return $query;
  }

  /**
   * Gets a field item list.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   Returns a FieldItemListInterface.
   */
  public function getFieldItemList(FieldableEntityInterface $entity) {
    foreach ($entity->getFieldDefinitions() as $definition) {
      if ($definition->getType() === 'bridtv') {
        $field = $definition->getName();
        return $entity->get($field);
      }
    }
    return NULL;
  }

  /**
   * Gets the video data.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   * @param bool $decode
   *   If it should be decoded or not.
   *
   * @return array|null
   *   Returns an array of data.
   */
  public function getVideoData(FieldableEntityInterface $entity, $decode = TRUE) {
    if ($items = $this->getFieldItemList($entity)) {
      if (!$items->isEmpty()) {
        /** @var \Drupal\bridtv\Plugin\Field\FieldType\BridtvItem $item */
        $item = $items->first();
        return $item->getBridApiData($decode);
      }
    }
    return NULL;
  }

  /**
   * Using Internal Cache.
   */
  protected function usingInternalCache() {
    $this->cacheResetCounter++;
    if ($this->cacheResetCounter > 100) {
      $this->cacheResetCounter = 0;
      $this->cached = [$this->type => []];
      $this->getEntityStorage()->resetCache();
      $this->entityMemoryCache->deleteAll();
    }
  }

}
