<?php

namespace Drupal\bridtv;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;

/**
 * The Brid.TV information negotiator.
 *
 * This service delivers required information either from the
 * local storage, or from the Brid.TV service provider when
 * data is not yet existing in the local storage.
 */
class BridInfoNegotiator {

  /**
   * The Brid.TV settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * The Brid API consumer.
   *
   * @var \Drupal\bridtv\BridApiConsumer
   */
  protected $consumer;

  /**
   * The entity resolver.
   *
   * @var \Drupal\bridtv\BridEntityResolver
   */
  protected $entityResolver;

  /**
   * The key value storage.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * BridInfoNegotiator constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\bridtv\BridApiConsumer $consumer
   *   The Brid API consumer.
   * @param \Drupal\bridtv\BridEntityResolver $entity_resolver
   *   The entity resolver.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $kv_factory
   *   The key value factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, BridApiConsumer $consumer, BridEntityResolver $entity_resolver, KeyValueFactoryInterface $kv_factory) {
    $this->settings = $config_factory->get('bridtv.settings');
    $this->consumer = $consumer;
    $this->entityResolver = $entity_resolver;
    $this->keyValue = $kv_factory->get('bridtv');
  }

  /**
   * Gets the video data.
   */
  public function getVideoData($id, $decode = TRUE) {
    $resolver = $this->entityResolver;
    if ($entity = $resolver->getEntityForVideoId($id)) {
      if ($data = $resolver->getVideoData($entity, $decode)) {
        return $data;
      }
    }
    $consumer = $this->consumer;
    if ($consumer->isReady()) {
      return $decode ? $consumer->getDecodedVideoData($id) : $consumer->fetchVideoData($id);
    }
    return NULL;
  }

  /**
   * Gets the players list.
   */
  public function getPlayersList($decode = TRUE) {
    if (!($value = $this->keyValue->get('players_list'))) {
      $consumer = $this->consumer;
      if ($consumer->isReady() && ($value = $consumer->fetchPlayersList())) {
        if (BridSerialization::decode($value)) {
          $this->keyValue->set('players_list', $value);
        }
      }
    }
    if ($value) {
      return $decode ? BridSerialization::decode($value) : $value;
    }
    return [];
  }

  /**
   * Gets the players data.
   */
  public function getPlayerData($id, $decode = TRUE) {
    $key = 'player_data.' . $id;
    $player = $this->keyValue->get($key);
    if (!$player) {
      $consumer = $this->consumer;
      if ($consumer->isReady() && ($player = $consumer->fetchPlayerData($id))) {
        $this->keyValue->set($key, $player);
      }
    }
    if ($player) {
      return $decode ? BridSerialization::decode($player) : $player;
    }
    return NULL;
  }

  /**
   * Gets the players list options.
   */
  public function getPlayersListOptions() {
    return $this->getPlayersList();
  }

  /**
   * Get the player sizes (width and height).
   *
   * @param int $id
   *   The player ID.
   *
   * @return array
   *   Either an array having width and height key,
   *   or an empty array if not available.
   */
  public function getPlayerSizes($id) {
    $key = 'player.' . $id . '.size';
    $value = $this->keyValue->get($key);
    if (!$value) {
      $value = [];
      $player = $this->getPlayerData($id);
      if (!empty($player)) {
        if (!empty($player['PlayerTemplate']['width']) && !empty($player['PlayerTemplate']['height'])) {
          $value = [
            'width' => $player['PlayerTemplate']['width'],
            'height' => $player['PlayerTemplate']['height'],
          ];
          $this->keyValue->set($key, $value);
        }
      }
    }
    return $value;
  }

  /**
   * Get the detault player id.
   *
   * @return int|null
   *   Returns the player id.
   */
  public function getDefaultPlayerId() {
    $player_id = (string) $this->settings->get('default_player');
    $players = $this->getPlayersListOptions();
    if ($player_id && isset($players[$player_id])) {
      return (int) $player_id;
    }
    if (!empty($players)) {
      $player_ids = array_keys($players);
      return reset($player_ids);
    }
    return NULL;
  }

}
