<?php

namespace Drupal\bridtv;

use Drupal\Component\Utility\Xss;

/**
 * Class for representing a specific video instance to embed.
 *
 * The instance consists of any known information
 * required for embedding the video on a page.
 * Its information can be altered any time, until
 * it's being finally displayed.
 */
class BridEmbeddingInstance extends BridEmbeddingInstanceBase {

  /**
   * Get the Brid.TV video id.
   *
   * @return int|null
   *   The video id.
   */
  public function getVideoId() {
    return $this->get('video_id');
  }

  /**
   * Returns the description for being displayed.
   *
   * @return string
   *   The description as safe markup.
   */
  public function getDescriptionOutput() {
    if (!($text = $this->get('description'))) {
      $text = '';
    }
    if (!empty($this->settings['format'])) {
      $text = (string) check_markup($text, $this->settings['format']);
    }
    else {
      $text = trim(Xss::filter(strip_tags($text)));
    }
    return $text;
  }

  /**
   * Returns if description output is enabled or not.
   *
   * @param null $enabled
   *   Enabled.
   *
   * @return bool
   *   Enabled or not.
   */
  public function isDescriptionOutputEnabled($enabled = NULL) {
    if (isset($enabled)) {
      $this->settings['show_desc'] = !empty($enabled);
    }
    return !empty($this->settings['show_desc']);
  }

}
