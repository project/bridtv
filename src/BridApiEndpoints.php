<?php

namespace Drupal\bridtv;

/**
 * Helper class to build any known Brid.TV url endpoints.
 */
abstract class BridApiEndpoints {

  const BASE_URL = 'https://api.brid.tv/apiv3/';

  /**
   * Returns the endpoint url to GET video information.
   *
   * @param int $id
   *   The video id.
   *
   * @return string
   *   The corresponding endpoint url.
   *
   * @see https://developer.brid.tv/reference/videoview
   */
  public static function video($id) {
    return static::BASE_URL . 'video/view/' . $id . '.json';
  }

  /**
   * Returns the endpoint url to GET a paginated list of videos.
   *
   * @param int $id
   *   The site id, which can be the partner_id from settings.
   * @param int $page
   *   The page to navigate to. Default would be the first page.
   * @param int $limit
   *   The maximum number of videos to fetch. Default is 5 items.
   *
   * @return string
   *   The corresponding endpoint url.
   *
   * @see https://developer.brid.tv/reference/videolist
   */
  public static function videosList($id, $page = 1, $limit = 5) {
    return static::BASE_URL . sprintf('video/list/%d.json?page=%d&limit=%d', $id, $page, $limit);
  }

  /**
   * Returns the endpoint url to GET a list of players.
   *
   * @param int $id
   *   The site id, which can be the partner_id from settings.
   *
   * @return string
   *   The corresponding endpoint url.
   */
  public static function playersList($id) {
    return static::BASE_URL . 'player/list/' . $id . '.json';
  }

  /**
   * Returns the endpoint url to GET the player information.
   *
   * @param int $id
   *   The site id, which can be the partner_id from settings.
   *
   * @return string
   *   The corresponding endpoint url.
   *
   * @see https://developer.brid.tv/reference/viewplayer
   */
  public static function player($id) {
    return static::BASE_URL . 'player/view/' . $id . '.json';
  }

  /**
   * Returns the endpoint url to GET a paginated list of playlists.
   *
   * @param int $id
   *   The site id, which can be the partner_id from settings.
   * @param int $page
   *   The page to navigate to. Default would be the first page.
   * @param int $limit
   *   The maximum number of playlists to fetch. Default is 5 items.
   *
   * @return string
   *   The corresponding endpoint url.
   *
   * @see https://developer.brid.tv/reference/playlistlist
   */
  public static function playlistsList($id, $page = 1, $limit = 5) {
    return static::BASE_URL . sprintf('playlist/list/%d.json?page=%d&limit=%d', $id, $page, $limit);
  }

  /**
   * Returns the endpoint url to GET playlist information.
   *
   * @param int $id
   *   The playlist id.
   *
   * @return string
   *   The corresponding endpoint url.
   *
   * @see https://developer.brid.tv/reference/playlistview
   */
  public static function playlist($id) {
    return static::BASE_URL . 'playlist/view/' . $id . '.json';
  }

}
