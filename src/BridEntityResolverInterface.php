<?php

namespace Drupal\bridtv;

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Interface Brid Entity Resolver Interface.
 *
 * @package Drupal\bridtv
 */
interface BridEntityResolverInterface {

  /**
   * Creates a new entity instance.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   *   A new entity, unsaved, without any field values.
   */
  public function newEntity();

  /**
   * Get the entity type definition.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface|null
   *   Returns a EntityTypeInterface.
   */
  public function getEntityTypeDefinition();

  /**
   * Gets the entity storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   Returns a EntityStorageInterface.
   */
  public function getEntityStorage();

  /**
   * Gets the entity query.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   Returns a QueryInterface.
   */
  public function getEntityQuery();

  /**
   * Gets the field item list.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   Retruns a FieldItemListInterface.
   */
  public function getFieldItemList(FieldableEntityInterface $entity);

  /**
   * Gets the video data.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   * @param bool $decode
   *   Decode or not.
   *
   * @return array|null
   *   Returns an array of video data.
   */
  public function getVideoData(FieldableEntityInterface $entity, $decode = TRUE);

}
