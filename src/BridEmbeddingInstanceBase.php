<?php

namespace Drupal\bridtv;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;

/**
 * Base class for representing a Brid playlist/video.
 *
 * The instance consists of any known information
 * required for embedding the video on a page.
 * Its information can be altered any time, until
 * it's being finally displayed.
 */
class BridEmbeddingInstanceBase {

  /**
   * Embedding instance type: video/playlist.
   *
   * @var string
   */
  protected $type;

  /**
   * The entity representaion of the Brid.TV video or playlist.
   *
   * @var \Drupal\Core\Entity\FieldableEntityInterface
   */
  protected $entity;

  /**
   * The currently used Brid.TV player id.
   *
   * @var int
   */
  protected $playerId;

  /**
   * In-memory cached values.
   *
   * @var array
   */
  protected $cached = [];

  /**
   * The settings.
   *
   * @var array
   */
  protected $settings = [];

  /**
   * BridEmbeddingInstance constructor.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity representaion of the Brid.TV video or playlist.
   * @param int $player_id
   *   The Brid.TV player id.
   * @param array $settings
   *   (Optional) Custom settings to use.
   */
  public function __construct(FieldableEntityInterface $entity, $player_id, array $settings = []) {
    $this->setEntity($entity);
    $this->setPlayerId($player_id);
    $this->setSettings($settings);
    $this->type = 'video';
  }

  /**
   * Get the instance type.
   *
   * @return string
   *   Video or playlist.
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Get the entity representation of the Brid.TV video.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   *   Returns a fieldable entity interface.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Set the entity representation of the Brid.TV video or playlist.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   */
  public function setEntity(FieldableEntityInterface $entity) {
    $this->entity = $entity;
    $this->cached = [];
  }

  /**
   * Get the currently used player id.
   *
   * @return int|null
   *   The player id.
   */
  public function getPlayerId() {
    return $this->playerId;
  }

  /**
   * Set the Brid.TV player id to use.
   *
   * @param int $player_id
   *   The player id to use.
   */
  public function setPlayerId($player_id) {
    $this->playerId = $player_id;
  }

  /**
   * Gets the settings.
   *
   * @return array
   *   Returns the settings array.
   */
  public function getSettings() {
    return $this->settings;
  }

  /**
   * Set custom settings.
   *
   * @param array $settings
   *   The settings.
   */
  public function setSettings(array $settings) {
    $this->settings = $settings + [
      'show_desc' => TRUE,
      'format' => NULL,
      'width' => 1405,
      'height' => 750,
    ];
  }

  /**
   * Get a field value from the Brid.TV item.
   *
   * @param string $name
   *   The name of the value to get.
   *
   * @return mixed
   *   The value, if any.
   */
  public function get($name) {
    $entity = $this->entity;
    if (empty($this->cached[$name])) {
      $resolver = $this->getEntityResolver();
      $this->cached[$name] = FALSE;
      if (($resolver::ENTITY_TYPE === 'media') && ($entity->getEntityTypeId() === $resolver::ENTITY_TYPE)) {
        $this->cached[$name] = $entity->getSource()->getMetadata($entity, $name);
      }
      else {
        foreach ($entity->getFieldDefinitions() as $field => $definition) {
          if ($definition->getSource() === $resolver::FIELD) {
            if (!$entity->get($field)->isEmpty()) {
              try {
                $this->cached[$name] = $entity->get($field)->first()->get($name)->getValue();
              }
              catch (MissingDataException $e) {
              }
            }
            break;
          }
        }
      }
    }
    return $this->cached[$name] ? $this->cached[$name] : NULL;
  }

  /**
   * Gets the entity resolver.
   *
   * @return \Drupal\bridtv\BridEntityResolver
   *   Returns the BridEntityResolver.
   */
  protected function getEntityResolver() {
    return \Drupal::service('bridtv.entities');
  }

}
