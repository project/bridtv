<?php

namespace Drupal\bridtv;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Queue\RequeueException;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Abstract base  class for handling synchronization of video data.
 */
abstract class BridSyncBase {

  use StringTranslationTrait;

  /**
   * Lease time of claimed queue items.
   */
  const LEASE_TIME = 60;

  /**
   * The default number of video items to process per queue item.
   */
  const ITEMS_PER_QUEUE_ITEM = 5;

  /**
   * The queue holding sync items.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * The corresponding queue worker.
   *
   * @var \Drupal\Core\Queue\QueueWorkerInterface
   */
  protected $worker;

  /**
   * The settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * The consumer.
   *
   * @var \Drupal\bridtv\BridApiConsumer
   */
  protected $consumer;

  /**
   * The key value storage.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * The Brid Entity Resolver Interface.
   *
   * @var \Drupal\bridtv\BridEntityResolverInterface
   */
  protected $entityResolver;

  /**
   * Sets the Auto create Enabled.
   *
   * @var bool
   */
  protected $autocreateEnabled;

  /**
   * Sets the Auto delete Enabled.
   *
   * @var bool
   */
  protected $autodeleteEnabled;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * BridSyncBase constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory interface.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory interface.
   * @param \Drupal\bridtv\BridApiConsumer $consumer
   *   The brid api consumer.
   * @param \Drupal\bridtv\BridEntityResolverInterface $entity_resolver
   *   The brid entity resolver interface.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $kv_factory
   *   The key value factory interface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger interface.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_factory, BridApiConsumer $consumer, BridEntityResolverInterface $entity_resolver, KeyValueFactoryInterface $kv_factory, MessengerInterface $messenger) {
    $this->settings = $config_factory->get('bridtv.settings');
    $this->logger = $logger_factory->get('bridtv');
    $this->messenger = $messenger;
    $this->consumer = $consumer;
    $this->entityResolver = $entity_resolver;
    $this->keyValue = $kv_factory->get('bridtv');
    $this->autocreateEnabled(!empty($this->settings->get('sync_autocreate')));
    $this->autodeleteEnabled(!empty($this->settings->get('sync_autodelete')));
  }

  /**
   * Sync players info.
   */
  public function syncPlayersInfo() {
    // Delete all player data.
    $this->keyValue->deleteAll();

    // Sync Player list.
    $consumer = $this->consumer;
    $player_list = [];
    if ($consumer->isReady() && $decoded = $consumer->getDecodedPlayersList()) {
      if (isset($decoded['data'])) {
        $player_list = $decoded['data'];
        $this->keyValue->set('players_list', BridSerialization::encode($player_list));
      }
    }

    // Sync Player data for each player in list.
    // TODO: If there are too many players defined, this may timeout therefore
    // it may need to be implemented as batch operation.
    foreach ($player_list as $id => $player) {
      $key = 'player_data.' . $id;
      if ($consumer->isReady() && ($player = $consumer->fetchPlayerData($id))) {
        $this->keyValue->set($key, $player);
      }
    }
  }

  /**
   * Run.
   */
  public function run($limit = -1) {
    $limit = (int) $limit;
    $this->prepareFullSync();
    for ($i = 0; $i !== $limit; $i++) {
      if (!$this->processNextItem()) {
        break;
      };
    }
  }

  /**
   * Process next item.
   */
  public function processNextItem() {
    $queue = $this->queue;
    $worker = $this->worker;

    if (!($item = $queue->claimItem(static::LEASE_TIME))) {
      $this->logger->info($this->t('No more items in the queue for processing synchronization.'));
      return FALSE;
    }

    try {
      $worker->processItem($item->data);
      $queue->deleteItem($item);
      return TRUE;
    }
    catch (RequeueException $e) {
      // The worker requested the task to be immediately requeued.
      $queue->releaseItem($item);
      return TRUE;
    }
    catch (SuspendQueueException $e) {
      $queue->releaseItem($item);
      $queue->deleteQueue();
      watchdog_exception('bridtv', $e);
    }
    catch (\Exception $e) {
      // In case of any other kind of exception, log it and leave the item
      // in the queue to be processed again later.
      watchdog_exception('bridtv', $e);
    }
    $this->logger->error('The Brid.TV queue for video/playlist data synchronization has stopped working properly. See the Watchdog log messages for further messages.');
    return FALSE;
  }

  /**
   * Queue remote list item.
   */
  public function queueRemoteListItem($page, $queue_next) {
    $this->queue->createItem(['page' => $page, 'q_next' => $queue_next]);
  }

  /**
   * Queue entity item.
   */
  public function queueEntityItem($entity_id, $queue_next) {
    $this->queue->createItem([
      'entity_id' => $entity_id,
      'q_next' => $queue_next,
    ]);
  }

  /**
   * Queue multiple entities item.
   */
  public function queueMultipleEntitiesItem(array $entity_ids, $queue_next) {
    $this->queue->createItem([
      'entity_ids' => $entity_ids,
      'q_next' => $queue_next,
    ]);
  }

  /**
   * Get consumer.
   *
   * @return \Drupal\bridtv\BridApiConsumer
   *   Returns the BridApiConsumer.
   */
  public function getConsumer() {
    return $this->consumer;
  }

  /**
   * Returns the entity resolver.
   *
   * @return \Drupal\bridtv\BridEntityResolver
   *   Returns the BridEntityResolver.
   */
  public function getEntityResolver() {
    return $this->entityResolver;
  }

  /**
   * Checks if autocreate is enabled or not.
   *
   * @return bool
   *   Autocreate or not.
   */
  public function autocreateEnabled($autocreate = NULL) {
    if (isset($autocreate)) {
      $this->autocreateEnabled = !empty($autocreate);
    }
    return $this->autocreateEnabled;
  }

  /**
   * Checks if autocdelete is enabled or not.
   *
   * @return bool
   *   Autodelete or not.
   */
  public function autodeleteEnabled($autodelete = NULL) {
    if (isset($autodelete)) {
      $this->autodeleteEnabled = !empty($autodelete);
    }
    return $this->autodeleteEnabled;
  }

}
