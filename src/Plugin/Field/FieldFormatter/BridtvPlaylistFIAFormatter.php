<?php

namespace Drupal\bridtv\Plugin\Field\FieldFormatter;

/**
 * Formatter plugin for viewing Bridtv playlists via JavaScript.
 *
 * @FieldFormatter(
 *   id = "bridtv_playlist_fia",
 *   module = "bridtv",
 *   label = @Translation("FIA playlist player"),
 *   field_types = {
 *     "bridtv_playlist",
 *     "bridtv_reference"
 *   }
 * )
 */
class BridtvPlaylistFIAFormatter extends BridPlaylistFormatterBase {

  /**
   * The theme prepend.
   *
   * @var string
   */
  static protected $themePrepend = 'bridtv';

  /**
   * The theme append.
   *
   * @var string
   */
  static protected $themeAppend = '_fia';

}
