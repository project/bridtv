<?php

namespace Drupal\bridtv\Plugin\Field\FieldFormatter;

/**
 * Formatter for viewing Bridtv videos via Accelerated Mobile Pages (AMP).
 *
 * @FieldFormatter(
 *   id = "bridtv_amp",
 *   module = "bridtv",
 *   label = @Translation("Accelerated Mobile Pages (AMP)"),
 *   field_types = {
 *     "bridtv",
 *     "bridtv_reference"
 *   }
 * )
 */
class BridtvAMPFormatter extends BridFormatterBase {

  /**
   * The theme append.
   *
   * @var string
   */
  static protected $themeAppend = '_amp';

}
