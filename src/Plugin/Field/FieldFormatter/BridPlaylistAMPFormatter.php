<?php

namespace Drupal\bridtv\Plugin\Field\FieldFormatter;

/**
 * Formatter for viewing Bridtv playlists via Accelerated Mobile Pages (AMP).
 *
 * @FieldFormatter(
 *   id = "bridtv_playlist_amp",
 *   module = "bridtv",
 *   label = @Translation("Accelerated Mobile Pages (AMP)"),
 *   field_types = {
 *     "bridtv_playlist",
 *     "bridtv_reference"
 *   }
 * )
 */
class BridPlaylistAMPFormatter extends BridPlaylistFormatterBase {

  /**
   * The theme prepend.
   *
   * @var string
   */
  static protected $themePrepend = 'bridtv';

  /**
   * The theme append.
   *
   * @var string
   */
  static protected $themeAppend = '_amp';

}
