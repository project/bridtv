<?php

namespace Drupal\bridtv\Plugin\Field\FieldFormatter;

use Drupal\bridtv\BridInfoNegotiator;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base formatter class for viewing Bridtv videos.
 */
abstract class BridPlaylistFormatterBase extends BridFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The theme prepend.
   *
   * @var string
   */
  static protected $themePrepend = 'bridtv';

  /**
   * The theme append.
   *
   * @var string
   */
  static protected $themeAppend = '_js';

  /**
   * The Brid.TV negotiator service.
   *
   * @var \Drupal\bridtv\BridInfoNegotiator
   */
  protected $bridNegotiator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings']
    );
    $instance->setBridNegotiator($container->get('bridtv.negotiator'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [];
  }

  /**
   * Set the Brid.TV negotiator service.
   *
   * @param \Drupal\bridtv\BridInfoNegotiator $negotiator
   *   The negotiator.
   */
  public function setBridNegotiator(BridInfoNegotiator $negotiator) {
    $this->bridNegotiator = $negotiator;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /**
     * @var \Drupal\bridtv\Plugin\Field\FieldType\BridtvPlaylist $item
     */
    foreach ($items as $delta => $item) {
      if ($embedding = $item->getBridEmbeddingInstance()) {
        $elements[$delta] = [
          '#theme' => $this->getTheme($embedding),
          '#cache' => [
            'tags' => $embedding->getEntity()->getCacheTags(),
          ],
        ];
      }
    }

    return $elements;
  }

}
