<?php

namespace Drupal\bridtv\Plugin\Field\FieldFormatter;

/**
 * Formatter plugin for viewing Bridtv playlists via JavaScript.
 *
 * @FieldFormatter(
 *   id = "bridtv_playlist_js",
 *   module = "bridtv",
 *   label = @Translation("JavaScript playlist player"),
 *   field_types = {
 *     "bridtv_playlist",
 *     "bridtv_reference"
 *   }
 * )
 */
class BridtvPlaylistJsFormatter extends BridPlaylistFormatterBase {

}
