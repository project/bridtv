<?php

namespace Drupal\bridtv\Plugin\Field\FieldWidget;

use Drupal\bridtv\BridApiConsumer;
use Drupal\bridtv\BridInfoNegotiator;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Video ID widget for Brid.TV videos.
 *
 * @FieldWidget(
 *   id = "bridtv_playlist_id",
 *   module = "bridtv",
 *   label = @Translation("Direct playlist ID input"),
 *   field_types = {
 *     "bridtv_playlist"
 *   }
 * )
 */
class BridtvPlaylistIdWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The Brid.TV API consumer service.
   *
   * @var \Drupal\bridtv\BridApiConsumer
   */
  protected $consumer;

  /**
   * The negotiator service.
   *
   * @var \Drupal\bridtv\BridInfoNegotiator
   */
  protected $negotiator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $consumer = $container->get('bridtv.consumer');
    $negotiator = $container->get('bridtv.negotiator');
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $consumer,
      $negotiator
    );
  }

  /**
   * Constructs a BridtvIdWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\bridtv\BridApiConsumer $brid_api
   *   The Brid.TV API service.
   * @param \Drupal\bridtv\BridInfoNegotiator $negotiator
   *   The Brid Info Negotiator.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, BridApiConsumer $brid_api, BridInfoNegotiator $negotiator) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->consumer = $brid_api;
    $this->negotiator = $negotiator;
  }

  /**
   * Options for the shuffle.
   *
   * @return string[]
   *   Returns an array of options.
   */
  private function getShuffleOptions() {
    return [0 => 'Off', 1 => 'On'];
  }

  /**
   * Options for widget positions.
   *
   * @return string[]
   *   Returns an array of options.
   */
  private function getWidgetPositionOptions() {
    return [
      0 => 'Default behavior. Will display playlist thumbs below the player.',
      1 => 'Will display playlist thumbs to the left of the player.',
      2 => 'Will display playlist thumbs to the right of the player.',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $values = $items->get($delta)->getValue();
    $element['playlist_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Playlist ID'),
      '#description' => !empty($values['playlist_id']) ? $this->t('You cannot change the playlist ID, as it might be used by others already.') : $this->t('Playlist ID number.'),
      '#default_value' => !empty($values['playlist_id']) ? $values['playlist_id'] : NULL,
      '#disabled' => !empty($values['playlist_id']),
    ];

    $element['player'] = [
      '#type' => 'select',
      '#title' => $this->t('Player'),
      '#options' => ['_use_default' => $this->t('- Use default -')] + $this->negotiator->getPlayersListOptions(),
      '#default_value' => !empty($values['player']) ? $values['player'] : '_use_default',
      '#description' => $this->t('Choose the player to use as default, when not specified otherwise.'),
    ];

    $element['shuffle'] = [
      '#type' => 'radios',
      '#title' => $this->t('Shuffle'),
      '#options' => $this->getShuffleOptions(),
      '#default_value' => !empty($values['shuffle']) ? $values['shuffle'] : 0,
      '#description' => $this->t('TUrn on or off the shuffle optio.'),
    ];

    $element['widget_position'] = [
      '#type' => 'radios',
      '#title' => $this->t('Widget position'),
      '#options' => $this->getWidgetPositionOptions(),
      '#default_value' => !empty($values['widget_position']) ? $values['widget_position'] : 0,
      '#description' => $this->t('Choose the playlist widget position'),
    ];

    $element['#element_validate'][] = [get_class($this), 'validateElement'];
    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $form_state->getFormObject()->getEntity();
    $field = NULL;
    $test = $entity->getFieldDefinitions();
    foreach ($entity->getFieldDefinitions() as $definition) {
      if ($definition->getType() == 'bridtv_playlist') {
        $field = $definition->getName();
        break;
      }
    }
    $id_as_input = (int) $element['playlist_id']['#value'];
    if (!$id_as_input) {
      $form_state->setError($element['playlist_id'], t('No valid number is given.'));
    }

    if ($field) {
      $previous_id_value = !$entity->get($field)
        ->isEmpty() ? $entity->get($field)
        ->first()
        ->get('playlist_id')
        ->getValue() : NULL;
      if (!($id_as_input == $previous_id_value)) {
        $storage = \Drupal::entityTypeManager()
          ->getStorage($entity->getEntityTypeId());
        $query = $storage->getQuery();
        $query->accessCheck(TRUE);
        $existing = $query->condition($field . '.playlist_id', $id_as_input)
          ->range(0, 1)
          ->accessCheck()
          ->execute();
        if (!empty($existing)) {
          $existing = reset($existing);
          $existing = $storage->load($existing);
          $form_state->setError($element['playlist_id'], t('There is already an <a href=":existing" target="_blank">existing @type entity</a> for the requested playlist Id.', [
            ':existing' => $existing->toUrl()
              ->toString(),
            '@type' => $entity->getEntityType()->getLabel(),
          ]));
        }
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $massaged = [];
    foreach ($values as $value) {
      if (!empty($value['playlist_id'])) {
        $massaged[] = [
          'playlist_id' => $value['playlist_id'],
          'player' => $value['player'] ?? NULL,
          'shuffle' => $value['shuffle'] ?? 0,
          'widget_position' => $value['widget_position'] ?? 0,
        ];

      }
    }
    return $massaged;
  }

}
