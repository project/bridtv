<?php

namespace Drupal\bridtv\Plugin\Field\FieldType;

use Drupal\bridtv\BridSerialization;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin definition of the Brid.TV playlist item.
 *
 * @FieldType(
 *   id = "bridtv_playlist",
 *   label = @Translation("Brid.TV Playlist"),
 *   description = @Translation("Stores the information for a Brid.TV playlist,
 *   including any retrieved metadata."), category = @Translation("Media"),
 *   default_widget = "bridtv_playlist_id",
 *   default_formatter = "bridtv_js",
 *   module = "bridtv"
 * )
 */
class BridtvPlaylist extends FieldItemBase {

  /**
   * The decoded data retrieved from Brid.TV.
   *
   * @var array
   */
  protected $decodedData;

  /**
   * Get the decoded data retrieved from Brid.TV.
   *
   * @return array|null
   *   The decoded data as array, or NULL if not given.
   */
  public function getBridApiData($decode = TRUE) {
    if ($this->isEmpty()) {
      return NULL;
    }
    if ($decode) {
      if (!isset($this->decodedData)) {
        $this->decodedData = BridSerialization::decode($this->get('data')
          ->getValue());
      }
      return $this->decodedData ?? NULL;
    }
    return $this->get('data')->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['playlist_id'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('The playlist id at Brid.TV.'))
      ->setRequired(TRUE);
    $properties['title'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('The playlist title.'));
    $properties['player'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Brid.TV Player id'));
    $properties['shuffle'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('SHuffle option'));
    $properties['widget_position'] = DataDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Widget position'));
    $properties['publish_date'] = DataDefinition::create('datetime_iso8601')
      ->setLabel(new TranslatableMarkup('Date value'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [];
    $schema['columns'] = [
      'playlist_id' => [
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'name' => [
        'type' => 'varchar',
        'length' => 187,
      ],
      'player' => [
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
      ],
      'shuffle' => [
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
      ],
      'widget_position' => [
        'type' => 'int',
        'size' => 'normal',
        'unsigned' => TRUE,
      ],
      'publish_date' => [
        'type' => 'varchar',
        'length' => 20,
      ],
    ];
    $schema['indexes'] = [
      'i_playlist_id' => ['playlist_id'],
    ];
    $schema['unique keys'] = [
      'u_playlist_id' => ['playlist_id'],
    ];
    return $schema;
  }

}
