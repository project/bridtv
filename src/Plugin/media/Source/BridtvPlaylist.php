<?php

namespace Drupal\bridtv\Plugin\media\Source;

use Drupal\Core\Form\FormStateInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Brid.TV Video type.
 *
 * @MediaSource(
 *   id = "bridtv_playlist",
 *   label = @Translation("Brid.TV Playlist"),
 *   description = @Translation("This is a Brid.TV playlist."),
 *   allowed_field_types = {"bridtv_playlist"},
 * )
 */
class BridtvPlaylist extends MediaSourceBase {

  /**
   * A list of valid field names to get values for.
   *
   * @var array
   */
  static protected $validNames = [
    'playlist_id' => 'Brid.TV Playlist Id',
    'title' => ' Playlist title',
    'publish_date' => 'Publish date',
  ];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    $fields = [];
    foreach (static::$validNames as $field => $label) {
      if ($label) {
        $fields[$field] = $this->t('@label', ['@label' => $label]);
      }
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $name) {
    if (empty($this->configuration['source_field'])) {
      return NULL;
    }
    $data_field = $this->configuration['source_field'];
    switch ($name) {
      case 'default_name':
        if ($title = $media->get($data_field)
          ->first()
          ->get('title')
          ->getValue()) {
          return $title;
        }
        return parent::getMetadata($media, 'default_name');

      default:
        if ($media->hasField($data_field) && !$media->get($data_field)
          ->isEmpty()) {
          if (isset(static::$validNames[$name])) {
            return $media->get($data_field)->first()->get($name)->getValue();
          }
        }
        break;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    return $form;
  }

}
