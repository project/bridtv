<?php

namespace Drupal\bridtv\Plugin\media\Source;

use Drupal\bridtv\BridResources;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Brid.TV Video type.
 *
 * @MediaSource(
 *   id = "bridtv",
 *   label = @Translation("Brid.TV Video"),
 *   description = @Translation("This is a Brid.TV Video."),
 *   allowed_field_types = {"bridtv"},
 *   default_thumbnail_filename = "video.png"
 * )
 */
class Bridtv extends MediaSourceBase {

  /**
   * The brid resources service.
   *
   * @var \Drupal\bridtv\BridResources
   */
  protected $bridResources;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The Http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * A list of valid field names to get values for.
   *
   * @var array
   */
  static protected $validNames = [
    'video_id' => 'Brid.TV Video Id',
    'title' => 'Video title',
    'description' => 'Video description',
    'publish_date' => 'Publish date',
    'data' => FALSE,
  ];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setBridResources($container->get('bridtv.resources'));
    $instance->setHttpClient($container->get('http_client'));
    $instance->setFileSystem($container->get('file_system'));
    return $instance;
  }

  /**
   * Set the Brid.TV resources service.
   *
   * @param \Drupal\bridtv\BridResources $resources
   *   The Brid.TV resources service.
   */
  protected function setBridResources(BridResources $resources) {
    $this->bridResources = $resources;
  }

  /**
   * Set file system.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   */
  protected function setFileSystem(FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * Set the Http client.
   *
   * @param \GuzzleHttp\Client $client
   *   The Http client.
   */
  protected function setHttpClient(Client $client) {
    $this->httpClient = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    $fields = [];
    foreach (static::$validNames as $field => $label) {
      if ($label) {
        $fields[$field] = $this->t('@label', ['@label' => $label]);
      }
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $name) {
    if (empty($this->configuration['source_field'])) {
      return NULL;
    }
    $data_field = $this->configuration['source_field'];
    switch ($name) {
      case 'default_name':
        if ($title = $media->get($data_field)->first()->get('title')->getValue()) {
          return $title;
        }
        return parent::getMetadata($media, 'default_name');

      case 'thumbnail_uri':
        return $this->getThumbnailUri($media) ?: parent::getMetadata($media, 'thumbnail_uri');

      default:
        if ($media->hasField($data_field) && !$media->get($data_field)->isEmpty()) {
          if (isset(static::$validNames[$name])) {
            return $media->get($data_field)->first()->get($name)->getValue();
          }
        }
        break;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getThumbnailUri(MediaInterface $media) {
    if (!empty($this->configuration['local_images_uri']) && !empty($this->configuration['source_field'])) {
      $data_field = $this->configuration['source_field'];
      if ($media->hasField($data_field) && !$media->get($data_field)->isEmpty()) {
        $video_id = $this->getMetadata($media, 'video_id');
        $local_file_uri = $this->configuration['local_images_uri'] . '/thumbnail/' . $video_id . '.jpg';
        if (file_exists($local_file_uri)) {
          return $local_file_uri;
        }

        if ($data = $media->get($data_field)->first()->getBridApiData()) {
          if (!empty($data['Video']['image'])) {
            $remote_image_uri = $data['Video']['image'];
            if (!strpos($remote_image_uri, '://') && !(strpos($remote_image_uri, '//') === 0)) {
              $remote_image_uri = $this->bridResources->getSnaphotUrlFor($remote_image_uri);
            }
            $response = $this->httpClient->get($remote_image_uri);
            if ($response->getStatusCode() == 200) {
              $directory = $this->configuration['local_images_uri'] . '/thumbnail';
              if (!is_dir($directory)) {
                $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
              }
              $this->fileSystem->saveData((string) $response->getBody(), $local_file_uri, FileSystemInterface::EXISTS_REPLACE);
            }
            if (file_exists($local_file_uri)) {
              return $local_file_uri;
            }
          }
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['local_images_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Uri for locally stored images'),
      '#description' => $this->t('The uri location for storing local images. Leave empty to not use local images.'),
      '#default_value' => empty($this->configuration['local_images_uri']) ? NULL : $this->configuration['local_images_uri'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultThumbnail() {
    return $this->config->get('icon_base') . '/video.png';
  }

}
