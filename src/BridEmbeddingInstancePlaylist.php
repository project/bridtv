<?php

namespace Drupal\bridtv;

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Class for representing a specific playlist instance to embed.
 *
 * The instance consists of any known information
 * required for embedding the playlist on a page.
 * Its information can be altered any time, until
 * it's being finally displayed.
 */
class BridEmbeddingInstancePlaylist extends BridEmbeddingInstanceBase {

  /**
   * The playlist field name.
   *
   * @var string
   */
  protected $playlistFieldName;

  /**
   * {@inheritdoc}
   */
  public function __construct(FieldableEntityInterface $entity, $player_id, array $settings = []) {
    parent::__construct($entity, $player_id, $settings);
    $fieldDefinitions = $entity->getFieldDefinitions();
    foreach ($fieldDefinitions as $fieldDefinition) {
      $type = $fieldDefinition->getType();
      $fieldName = $fieldDefinition->getName();
      // Store field name for retrieving fallback values.
      if ('bridtv_playlist' === $type && strpos($fieldName, 'field_media_') === 0) {
        // @todo Is there ever a use case of having multiple fields on media?
        $this->playlistFieldName = $fieldDefinition->getName();
        break;
      }
    }
    $this->type = 'playlist';
  }

  /**
   * Helper function for retrieving value from the Brid.TV Playlist.
   *
   * @param string $propertyName
   *   The property name.
   * @param null $defaultValue
   *   Default value.
   *
   * @return mixed|null
   *   Returns the value of playlist field property.
   */
  private function getValueOfPlaylistFieldProperty($propertyName, $defaultValue = NULL) {
    $value = $defaultValue;
    $fieldValues = $this->entity->get($this->playlistFieldName)->getValue();
    if (!empty($fieldValues) && $fieldValues = reset($fieldValues)) {
      return $fieldValues[$propertyName] ?? $defaultValue;
    }
    return $value;
  }

  /**
   * Get the Brid.TV playlist id.
   *
   * @return int|null
   *   The playlist id.
   */
  public function getPlaylistId() {
    return $this->get('playlist_id');
  }

  /**
   * Get the Brid.TV shuffle options.
   *
   * @return bool
   *   Shuffle on/off.
   */
  public function getShuffle() {
    return $this->getValueOfPlaylistFieldProperty('shuffle', 0);
  }

  /**
   * Get the Brid.TV widget positions options.
   *
   * @return int
   *   Widget position.
   */
  public function getWidgetPosition() {
    return $this->getValueOfPlaylistFieldProperty('widget_position', 0);
  }

}
