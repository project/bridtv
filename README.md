CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Features
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

Integrates the Brid.TV video service provider into Drupal.

* For a full description of the module, visit the project page:
https://www.drupal.org/project/bridtv

* To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/bridtv


REQUIREMENTS
------------

This module has no requirements, but integrates with he Media system and
Paragraphs, if installed.

The module has been initially build to work with the Thunder distribution
(drupal.org/project/thunder).
If you're using a different system and experience problems, feel free to report
your problems in the issue queue.


FEATURES
--------

Once installed, the module provides:

* A new media type "Brid.TV Video" (requires the Media system to be
installed).
* A new paragraph type "Brid.TV Embedded Video" (Paragraphs module required).
* Optional synchronization of the videos with the provider.
* for developers: multiple services for using video data from Brid.TV.

Further planned features (not existing yet):

* An entity browser for browsing through existing Brid.TV Videos
(requires the Entity Browser module to be installed).
* An upload section for directly uploading new videos to Brid.TV.


INSTALLATION
------------

* Install the Brid.TV Integration for Drupal module as you would normally
install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

You need credentials for an API authorization. The easiest way to do this
is to first login into the Brid.TV CMS (https://cms.brid.tv) and select your
site under the MANAGE VIDEOS option on the main left-hand menu. Once selected,
click on the SETTINGS option right below your domain name and you should see
the API section. See also
https://developer.brid.tv/brid-platform/php-api-client/authorization.


CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Configuration > Content Authoring > Brid.TV
setting for configuration.
3. Navigate to Administration > People > Permissions to configure
permissions.

Start synchronization of the Brid.TV data with your CMS. You can do this
by either using the UI via admin/bridtv/sync or by running the Drush
command bridtv-sync (Drush 8) or bridtv:sync (Drush 9).

Note:

Make sure the API credentials can only be read and accessed by
responsible persons. The credentials are being saved into the configuration,
without any encryption.

MAINTAINERS
-----------

* Maximilian Haupt (mxh) - https://www.drupal.org/u/mxh

Supporting organizations:

* Hubert Burda Media - https://www.drupal.org/hubert-burda-media
